# Conventions

This document describes various guidelines and best practices in Go programming based on my product developer experience.

Some rules based on using tool [architect](https://gitlab.com/zigal0/architect) and Gitlab as a code host.

## Code style and format

### Error
* If there is a need to define a custom error, it must contain a full description of the cause and logically match the name. The name itself must begin with Err, e.g. ErrNoEmployee.
* All errors unite in var blocks and take out separately in error.gi file.
* Errors should be wrapped all the time except some that return from little helpers. It helps to find the reason of error fast.  
```go
employee, err := m.employeeRepo.GetEmployeeByID(ctx, employeeID)
if err != nil {
    return nil, fmt.Errorf("employeeRepo.GetEmployeeByID: %w", err)
}
```
* If there is a need to define the wrap text, then the name should start with FormatErr and have a meaningful name.
* Errors should be checked via Is/As/Equal functions.
* Use business_error defined in architect for correct error message to other clients.
* Errors for client should not contain internal info of project. Only business info. 

### Interface
* Need to define interface where they are used. They should be private (visible only on pkg level). It decouple code dependencies. 
* Put all required interfaces in interface.go files. 
* Constructors return ref on struct, not interface.
* No need to create interface for every method. Such pattern is not working. So define one interface for entity with only required methods.

### SQL
* Write queries in clean SQL is a priority. Put them in separate file query.go in your repository.
* The name of query starts with query, e.g. queryGetEmployee.
* If query getting complicated it is good to put some comments that explain key logic.
* Select * is banned. Only for CTE queries: 
```postgresql
WITH cte AS (
    SELECT
        unnest($1::TEXT[]) 		AS name,
        unnest($2::INTEGER[]) 	AS age
)
INSERT INTO employee (name, age) SELECT * FROM cte
RETURNING id;
```
* ORM is banned. It is not for ABA pattern and in Go it is not common.
* If you require to build dynamic query based on some inout filter you can use [squirrel](https://github.com/Masterminds/squirrel). It is good to put full example query commented in query.go. 

### Testing
* Tests are always in separate packages, e.g. employee_test for employee pkg. 
* Use [minimock](https://github.com/gojuno/minimock) to generate mcoks. For this purpose you need to write next in your interface.go file.
```go
//go:generate sh -c "rm -rf mocks && mkdir -p mocks"
//go:generate minimock -i * -o ./mocks -s "_mock.go" -g
```
* No tests for api layer if it is just simple mapping domain to pb. 
* No tests for adapter layer (need to consider closely this issue due to existence of logic in repository).
* Use table-driven tests if it is possible. It is more maintainable and clean. 
```go
func Test_SomeMethod(t *testing.T) {
    t.Parallel()

    testCases := []struct {
        name            string
        input           []domain.SomeEntity
        expectedError   error
        expectedOutput  []domain.OtherEntity
    }{
		// cases
    }

    for _, testCase := range testCases {
        tc := testCase

        t.Run(tc.name, func(t *testing.T) {
            t.Parallel()

            // arrange
			
            // act
            output, err := f.logicEntity.SomeMethod(...)

            // assert
            if tc.expectedError != nil {
                f.ErrorIs(err, tc.expectedError)
            }
			
			f.Equal(tc.expectedOutput, output)
        })
    }
}
```
* It is good to use random values (e.g. via [faker](https://github.com/dmgk/faker)). However sometimes it better to put hardcoded values for future maintenance (imagine tests fail and every time different values).
* 100% coverage is not necessary and even bad. Cover only base flow and essential parts.

### Common
* Code == documentation, so choose names of entities, variables and methods carefully. 
* Try to put all logic in internal/business.
* Access to changeable values:
```go
WhiteListGetter: func() (string, error) {
    return config.GetValue(config.Key_WHITE_LIST)
}
```
* Keep number of return params as 1 or 2. More only for extra cases.
* If function takes more than 5 params - consider structRequest creation.
* If return value of method is alias for slice ```type Entities []Entity```, better to return Entities{} instead nil.
* Name of the dirs with underscores. 
* Update local_example.env if any changes in env files. Next developer doesn't want to think about this. 
* Remove /bin and /vendor.protogen before new generation or use ```make custom-generate``` command.
* User ```make pre-push``` command to check your code before push.
* Do not set ```nolint``` on all code if some linters argue but your solution is necessary. Just use ```nolint: lll``` e.g.
* Convert time to UTC format during work with Postgres. 
* Comments should be useful. They should explain why this code is required.

## Project architecture
You can see standard structure if projects in architect README.md. It is based on ABA architecture  (api-business-adapter).
* api - handlers without logic, only mapping and some parameter validation
* business - all business logic of your project, should be covered by tests
* adapter - external entities to work with data

It gives opportunity for fast and parallel development. let's consider simple task such as "Add handler with new entity". It can be divided into tasks:
* Add proto-contract, generation and mocking new handler (can unlock other team)
* Add migration (very important part)
* Add logic (mb more than one task)

## Project generation
* Create separate task for this purpose due to a lot of boilerplate code. It is hard tow watch review when this code mixed with some logic. 
* Set up Gitlab settings such as rules for MR & review (need to specify).
* Check version of architect. If it not last - update.
* Clone new repository and execute ```architect init 'module aka path to host platform'```. 
* Remove proto example if needed. 
* Create config/.env file anc check validity of generated code including docker build.
* Add example Makefile. Check all targets on your service, may be some are extra for you.
```makefile
include architect.mk

#======================================#
# VARIABLES
#======================================#

## GENERAL
SERVICE_NAME = "your-service"
MIGRATION_DIR = $(CURDIR)/migration
PG_DSN="host=localhost port=1331 user=$(SERVICE_NAME)-user password=1234 dbname=$(SERVICE_NAME) sslmode=disable"

## BIN
GOOSE_BIN = $(LOCAL_BIN)/goose

#======================================#
# INSTALLATION
#======================================#

.custom-bin-deps: export GOBIN := $(LOCAL_BIN)
.custom-bin-deps:
	$(info Installing custom bins for project...)    
	tmp=$$(mktemp -d) && cd $$tmp && go mod init temp && \
		go install github.com/pressly/goose/v3/cmd/goose@latest
		go install github.com/gojuno/minimock/v3/cmd/minimock@latest
		go install github.com/99designs/gqlgen@latest && \
	rm -rf $$tmp

custom-bin-deps: .custom-bin-deps ## install custom necessary bins

#======================================#
# GENERATION
#======================================#
 
.clean-before-generate:
    $(info Clean before generating...)
    @if [ -d $(CURDIR)/vendor.protogen ]; then \
        rm -rf $(CURDIR)/vendor.protogen; \
    fi
    @if [ -d $(CURDIR)/bin ]; then \
        rm -rf $(CURDIR)/bin; \
    fi

.generate-mocks:
	$(info Generating mocks...) 
	PATH="$(LOCAL_BIN):$$PATH" go generate -x -run minimock ./...
	
.generate-graphql:
    $(info Generating GraphQL code...)
    $(LOCAL_BIN)/gqlgen generate

generate-mocks: .generate-mocks ## generate mocks for tests

generate-graphql: ## generate code for GraphQL
    .generate-graphql
    
generate-custom: ## run generate after clean old bins & vendor
    @make .clean-before-generate
    @make generate

generate: .generate-graphql .generate-mocks .format-proto

# =============== #
# FORMAT
# =============== #
 
.format-proto:
    $(info Formatting proto files...)
    clang-format api/**/** -i -style="{BasedOnStyle: Google, ColumnLimit: 120, Language: Proto}"
    sed -i '' 's/\.repeated \./.repeated./g' api/**/**

#======================================#
# DOCKER
#======================================#

.compose-up: 
	docker compose -p $(SERVICE_NAME) -f ./local/docker/docker-compose.yml up -d

.compose-down: 
	docker compose -p $(SERVICE_NAME) -f ./local/docker/docker-compose.yml down
 
.compose-rm:
	docker compose -p $(SERVICE_NAME) -f ./local/docker/docker-compose.yml rm -fvs  #f - force, v - any anonymous volumes, s - stop

.compose-rs:
	make compose-down
	make compose-up

compose-up: .compose-up ## start docker containers

compose-down: .compose-down ## stop docker containers

compose-rm: .compose-rm ## stop and remove docker containers

compose-rs: .compose-rs ## restart docker containers

#======================================#
# MIGRATIONS
#======================================#
	
.migration-up: 
	$(GOOSE_BIN) -dir $(MIGRATION_DIR) postgres $(PG_DSN) up

.migration-down:
	$(GOOSE_BIN) -dir $(MIGRATION_DIR) postgres $(PG_DSN) down

migration-up: .migration-up ## run up pg migratins

migration-down: .migration-pg-down ## run down pg migratins

migration-create: ## create migration file in pg migration folder
	$(GOOSE_BIN) -dir $(MIGRATION_DIR) create $(name) sql

#======================================#
# BUILD & RUN
#======================================#

run-full: ## run service with db
	@make .compose-up
	@echo "Waiting for DBs to start"
	@sleep 15
	@make .migration-up
	@make .run

#======================================#
# .PHONY
#======================================#

.PHONY: \
    .custom-bin-deps \
    custom-bin-deps \
    .clean-before-generate \
    .generate-mocks \
    .generate-graphql \
    generate-mocks \
    generate-graphql \
    generate-custom \
    .format-proto \
    .compose-up \
    .compose-down \
    .compose-rm \
    .compose-rs \
    compose-up \
    compose-down \
    compose-rm \
    compose-rs \
    migration-create \
    .migration-up \
    .migration-down \
    migration-up \
    migration-down \
    run-full
```

## Git
* No direct commit to main/master. 
* For small tasks create branches like ```feature/ARCHITECT-1```. 
* For story (bunch of small tasks, general functional) create branches like ```release/new_functional```. All feature-branches should be merged into release-branches. After functional is ready - merge release to main/master.

## Process
* It is good to make time to time refactoring. It is always more clear how to organize your code after realization one of solution. However, if it is only workaround it can reflect in the future.  
