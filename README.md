# go-reference-project

Project with reference code examples. It is based on architect, so for more code you can go [here](https://gitlab.com/zigal0/architect).

For local development refer to Makefile and its commands. 

In conventions.md you can find a set of rules that are being developed based on my experience. 

## Useful links
* [Start Go](https://go.dev/tour/welcome/1)
* [The most useful link](https://github.com/avelino/awesome-go)
* [Go standards and style guidelines](https://docs.gitlab.com/ee/development/go_guide/)
* [Protobuf docs](https://protobuf.dev)
* [gRPC docs](https://grpc.io/docs/)
* [gRPC-Gateway docs](https://grpc-ecosystem.github.io/grpc-gateway/)

## Examples

### DB
- msql
- mssql
- mongo
- postgresql
- migration mechanism via [goose](https://github.com/pressly/goose)

### API
- gRPC
- REST + gRPC + Swagger via [rk-boot](https://github.com/rookie-ninja/rk-boot) // no custom middleware => no validation & etc.
- REST + gRPC + Swagger via [architect](https://gitlab.com/zigal0/architect)

### Other
- Makefile
- work with [minimock](https://github.com/gojuno/minimock) & [faker](https://github.com/dmgk/faker) for tests
- simple grpc client example
- simple grpc server example

## TODO 
- Kafka (consumer, producer, docker)
- ClickHouse 
- cache (reddis, Memcached)
- RabbitMQ
- cron (+1 to age for example)
- auth
- dao (single entry point for db)
- metrics (prometheus)
- Jaeger (spans)
- GraphQL