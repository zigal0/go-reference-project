package domain

import (
	"time"

	"cloud.google.com/go/civil"
)

type Task struct {
	ID         int64
	EmployeeID int64
	Action     string
	StartDate  civil.Date
	EndDate    civil.Date
	UpdatedAt  time.Time
	CreatedAt  time.Time
}
