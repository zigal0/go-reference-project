package domain

import "time"

type MongoEmployee struct {
	ID        string
	Name      string
	Age       int32
	Hobbies   []string
	CreatedAt time.Time
}
