package client

type Target string

const (
	GoRegerenceProject Target = "localhost:7002"
)

func (t Target) String() string {
	return string(t)
}
