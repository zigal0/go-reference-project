package go_reference_project_client

import (
	"time"

	"cloud.google.com/go/civil"
	"gitlab.com/zigal0/go-reference-project/internal/domain"
	pb "gitlab.com/zigal0/go-reference-project/internal/generated/api/employee_service"
	"google.golang.org/genproto/googleapis/type/date"
)

func taskFromPB(t *pb.Task) domain.Task {
	return domain.Task{
		ID:         t.GetId(),
		EmployeeID: t.GetEmployeeId(),
		Action:     t.GetAction(),
		StartDate:  civilDateFromPB(t.GetStartDate()),
		EndDate:    civilDateFromPB(t.GetEndDate()),
		UpdatedAt:  t.GetUpdateAt().AsTime(),
		CreatedAt:  t.GetCreatedAt().AsTime(),
	}
}

func civilDateFromPB(datePB *date.Date) civil.Date {
	return civil.Date{
		Year:  int(datePB.GetYear()),
		Month: time.Month(datePB.GetMonth()),
		Day:   int(datePB.GetDay()),
	}
}
