package go_reference_project_client

import (
	pb "gitlab.com/zigal0/go-reference-project/internal/generated/api/employee_service"
	"google.golang.org/grpc"
)

type Client struct {
	pbClient pb.EmployeeServiceClient
}

func New(conn *grpc.ClientConn) *Client {
	return &Client{
		pbClient: pb.NewEmployeeServiceClient(conn),
	}
}
