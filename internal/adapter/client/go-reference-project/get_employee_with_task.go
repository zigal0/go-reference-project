package go_reference_project_client

import (
	"context"
	"fmt"

	"gitlab.com/zigal0/go-reference-project/internal/business/tool/collection"
	"gitlab.com/zigal0/go-reference-project/internal/domain"
	pb "gitlab.com/zigal0/go-reference-project/internal/generated/api/employee_service"
)

func (c *Client) GetEmployeeWithTasks(
	ctx context.Context,
	employeeID int64,
) (domain.EmployeeWithTasks, error) {
	res, err := c.pbClient.GetEmployeeWithTasksV1(
		ctx,
		&pb.GetEmployeeWithTasksV1Request{EmployeeId: employeeID},
	)
	if err != nil {
		return domain.EmployeeWithTasks{}, fmt.Errorf("pbClient.GetEmployeeWithTasksV1: %w", err)
	}

	employee := res.GetEmployeeWithTasks().GetEmployee()
	tasks := res.GetEmployeeWithTasks().GetTasks()

	return domain.EmployeeWithTasks{
		Employee: domain.Employee{
			ID:        employee.GetId(),
			Name:      employee.GetName(),
			Age:       employee.GetAge(),
			CreatedAt: employee.CreatedAt.AsTime(),
		},
		Tasks: collection.Map(tasks, taskFromPB),
	}, nil
}
