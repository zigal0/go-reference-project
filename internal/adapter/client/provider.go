package client

import (
	"context"
	"fmt"

	"gitlab.com/zigal0/architect/pkg/closer"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func ProvideGRPCConn(ctx context.Context, target Target) (*grpc.ClientConn, error) {
	conn, err := grpc.DialContext(
		ctx,
		target.String(),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithBlock(),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to %s: %w", target, err)
	}

	closer.Add(conn.Close)

	return conn, nil
}
