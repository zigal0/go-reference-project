package repository

import "gitlab.com/zigal0/architect/pkg/logger"

type Closer interface {
	Close() error
}

func CloseWithLog(c Closer) {
	err := c.Close()
	if err != nil {
		logger.Errorf("failed to close: %v", err)
	}
}
