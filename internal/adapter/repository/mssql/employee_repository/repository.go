package employee_repostitory

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/zigal0/go-reference-project/internal/adapter/repository"
	"gitlab.com/zigal0/go-reference-project/internal/business/tool/collection"
	"gitlab.com/zigal0/go-reference-project/internal/domain"
)

type Repo struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *Repo {
	return &Repo{
		db: db,
	}
}

func (r *Repo) Truncate(ctx context.Context) error {
	_, err := r.db.ExecContext(
		ctx,
		queryTruncate,
	)
	if err != nil {
		return fmt.Errorf("db.ExecContext: %w", err)
	}

	return nil
}

func (r *Repo) GetByIDs(ctx context.Context, ids []int64) ([]domain.Employee, error) {
	if len(ids) == 0 {
		return nil, nil
	}

	var employees []Employee

	query, args, err := sqlx.In(queryGetByIDs, ids)
	if err != nil {
		return nil, fmt.Errorf("sqlx.In: %w", err)
	}

	query = r.db.Rebind(query)

	err = r.db.SelectContext(ctx, &employees, query, args...)
	if err != nil {
		return nil, fmt.Errorf("db.SelectContext: %w", err)
	}

	return collection.Map(employees, toDomain), nil
}

func (r *Repo) InsertBatch(ctx context.Context, employees []domain.Employee) ([]int64, error) {
	if len(employees) == 0 {
		return nil, nil
	}

	employeesDB := make([]Employee, 0, len(employees))

	for _, employee := range employees {
		employeesDB = append(employeesDB, Employee{
			Name: employee.Name,
			Age:  employee.Age,
		})
	}

	rows, err := r.db.NamedQueryContext(
		ctx,
		queryInsertBatch,
		employeesDB,
	)
	if err != nil {
		return nil, fmt.Errorf("db.NamedQueryContext: %w", err)
	}

	defer repository.CloseWithLog(rows)

	var ids []int64

	for rows.Next() {
		var id int64

		err := rows.Scan(&id)
		if err != nil {
			return nil, fmt.Errorf("rows.Scan: %w", err)
		}

		ids = append(ids, id)
	}

	return ids, nil
}
