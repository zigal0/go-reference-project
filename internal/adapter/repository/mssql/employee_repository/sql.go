package employee_repostitory

const (
	queryGetByIDs = `
SELECT 
	id, 
	name, 
	age,
	created_at 
FROM employee 
WHERE id IN (?)
`

	queryInsertBatch = `
DECLARE @output TABLE (id INT)

DECLARE @tmp TABLE (name VARCHAR(255), age INT)

INSERT INTO @tmp (name, age) VALUES (:name, :age)

INSERT INTO employee (name, age) 
OUTPUT inserted.ID INTO @output
SELECT * FROM @tmp

SELECT id FROM @output
`

	queryTruncate = `
TRUNCATE TABLE employee
`
)
