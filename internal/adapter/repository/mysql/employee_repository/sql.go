package employee_repository

const (
	queryGetByIDs = `
SELECT 
	id, 
	name, 
	age,
	created_at 
FROM employee 
WHERE id IN (?)
`

	queryInsertBatch = `
INSERT INTO employee (name, age) VALUES (:name, :age)
`

	queryTruncate = `
TRUNCATE employee
`
)
