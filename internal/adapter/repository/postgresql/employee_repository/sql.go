package employee_repository

const (
	queryGetByIDs = `
SELECT 
	id, 
	name, 
	age,
	created_at 
FROM employee 
WHERE id = ANY($1)
`

	queryInsertBatch = `
WITH cte AS (
	SELECT
		unnest($1::TEXT[]) 		AS name,
		unnest($2::INTEGER[]) 	AS age
)
INSERT INTO employee (name, age) SELECT * FROM cte
RETURNING id
`

	queryTruncate = `
TRUNCATE employee
`
)
