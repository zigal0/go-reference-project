package employee_repository

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/zigal0/go-reference-project/internal/adapter/repository"
	"gitlab.com/zigal0/go-reference-project/internal/business/tool/collection"
	"gitlab.com/zigal0/go-reference-project/internal/domain"
)

type Repo struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *Repo {
	return &Repo{
		db: db,
	}
}

func (r *Repo) Truncate(ctx context.Context) error {
	_, err := r.db.ExecContext(
		ctx,
		queryTruncate,
	)
	if err != nil {
		return fmt.Errorf("db.ExecContext: %w", err)
	}

	return nil
}

func (r *Repo) GetByIDs(ctx context.Context, ids []int64) ([]domain.Employee, error) {
	if len(ids) == 0 {
		return nil, nil
	}

	employees := []Employee{}

	err := r.db.SelectContext(
		ctx,
		&employees,
		queryGetByIDs,
		ids,
	)
	if err != nil {
		return nil, fmt.Errorf("db.SelectContext: %w", err)
	}

	return collection.Map(employees, toDomain), nil
}

func (r *Repo) InsertBatch(ctx context.Context, employees []domain.Employee) ([]int64, error) {
	if len(employees) == 0 {
		return nil, nil
	}

	var (
		nameSlice = make([]string, 0, len(employees))
		ageSlice  = make([]int32, 0, len(employees))
	)

	for _, employee := range employees {
		nameSlice = append(nameSlice, employee.Name)
		ageSlice = append(ageSlice, employee.Age)
	}

	rows, err := r.db.QueryContext(
		ctx,
		queryInsertBatch,
		nameSlice,
		ageSlice,
	)
	if err != nil {
		return nil, fmt.Errorf("db.QueryContext: %w", err)
	}

	defer repository.CloseWithLog(rows)

	var ids []int64

	for rows.Next() {
		var id int64

		err := rows.Scan(&id)
		if err != nil {
			return nil, fmt.Errorf("rows.Scan: %w", err)
		}

		ids = append(ids, id)
	}

	return ids, nil
}
