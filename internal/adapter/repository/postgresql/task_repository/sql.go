package task_repository

// subqueries & querry parts
const (
	taskFieldsToSelect = `
id,
employee_id,
action,
start_date,
end_date,
updated_at,
created_at
`
)

// queries
const (
	queryGetByIDs = `
SELECT` + taskFieldsToSelect + `
FROM task 
WHERE id = ANY($1)
`

	queryGetByEmployeeID = `
SELECT` + taskFieldsToSelect + `
FROM task 
WHERE employee_id = $1
`

	queryInsert = `
INSERT INTO task (action, start_date, end_date) 
VALUES ($1, $2, $3)
RETURNING id
`

	queryInsertBatch = `
WITH cte AS (
	SELECT
		unnest($1::TEXT[])	AS action,
		unnest($2::DATE[])	AS start_date,
		unnest($3::DATE[]) 	AS end_date
)
INSERT INTO task (action, start_date, end_date) SELECT * FROM cte
RETURNING id
`

	querySetEmployeeID = `
UPDATE task
SET
	employee_id = $1
WHERE
	id = $2
`

	queryTruncate = `
TRUNCATE task
`
)
