package task_repository

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/zigal0/go-reference-project/internal/adapter/repository"
	"gitlab.com/zigal0/go-reference-project/internal/business/tool/collection"
	"gitlab.com/zigal0/go-reference-project/internal/domain"
)

type Repo struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *Repo {
	return &Repo{
		db: db,
	}
}

func (r *Repo) Truncate(ctx context.Context) error {
	_, err := r.db.ExecContext(
		ctx,
		queryTruncate,
	)
	if err != nil {
		return fmt.Errorf("db.ExecContext: %w", err)
	}

	return nil
}

func (r *Repo) GetByIDs(ctx context.Context, ids []int64) ([]domain.Task, error) {
	if len(ids) == 0 {
		return nil, nil
	}

	tasks := []Task{}

	err := r.db.SelectContext(
		ctx,
		&tasks,
		queryGetByIDs,
		ids,
	)
	if err != nil {
		return nil, fmt.Errorf("db.SelectContext: %w", err)
	}

	return collection.Map(tasks, toDomain), nil
}

func (r *Repo) GetByEmployeeID(ctx context.Context, employeeID int64) ([]domain.Task, error) {
	tasks := []Task{}

	err := r.db.SelectContext(
		ctx,
		&tasks,
		queryGetByEmployeeID,
		employeeID,
	)
	if err != nil {
		return nil, fmt.Errorf("db.SelectContext: %w", err)
	}

	return collection.Map(tasks, toDomain), nil
}

func (r *Repo) Insert(ctx context.Context, task domain.Task) (int64, error) {
	row := r.db.QueryRowContext(
		ctx,
		queryInsert,
		task.Action,
		task.StartDate,
		task.EndDate,
	)
	if row.Err() != nil {
		return 0, fmt.Errorf("db.QueryRowContext: %w", row.Err())
	}

	var id int64

	err := row.Scan(&id)
	if err != nil {
		return 0, fmt.Errorf("row.Scan: %w", err)
	}

	return id, nil
}

func (r *Repo) InsertBatch(ctx context.Context, tasks []domain.Task) ([]int64, error) {
	qty := len(tasks)
	if qty == 0 {
		return nil, nil
	}

	var (
		actionSlice    = make([]string, 0, qty)
		startDateSlice = make([]time.Time, 0, qty)
		endDateSlice   = make([]time.Time, 0, qty)
	)

	for _, task := range tasks {
		actionSlice = append(actionSlice, task.Action)
		startDateSlice = append(startDateSlice, task.StartDate.In(time.UTC))
		endDateSlice = append(endDateSlice, task.EndDate.In(time.UTC))
	}

	rows, err := r.db.QueryContext(
		ctx,
		queryInsertBatch,
		actionSlice,
		startDateSlice,
		endDateSlice,
	)
	if err != nil {
		return nil, fmt.Errorf("db.QueryContext: %w", err)
	}

	defer repository.CloseWithLog(rows)

	var ids []int64

	for rows.Next() {
		var id int64

		err := rows.Scan(&id)
		if err != nil {
			return nil, fmt.Errorf("rows.Scan: %w", err)
		}

		ids = append(ids, id)
	}

	return ids, nil
}

func (r *Repo) SetEmployeeID(ctx context.Context, taskID int64, employeeID int64) error {
	_, err := r.db.ExecContext(
		ctx,
		querySetEmployeeID,
		employeeID,
		taskID,
	)
	if err != nil {
		return fmt.Errorf("db.ExecContext: %w", err)
	}

	return nil
}
