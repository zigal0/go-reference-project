package task_repository

import (
	"database/sql"
	"time"

	"cloud.google.com/go/civil"
	"gitlab.com/zigal0/go-reference-project/internal/domain"
)

type Task struct {
	ID         int64         `db:"id"`
	EmployeeID sql.NullInt64 `db:"employee_id"`
	Action     string        `db:"action"`
	StartDate  time.Time     `db:"start_date"`
	EndDate    time.Time     `db:"end_date"`
	UpdatedAt  time.Time     `db:"updated_at"`
	CreatedAt  time.Time     `db:"created_at"`
}

func toDomain(t Task) domain.Task {
	return domain.Task{
		ID:         t.ID,
		EmployeeID: t.EmployeeID.Int64,
		Action:     t.Action,
		StartDate:  civil.DateOf(t.StartDate),
		EndDate:    civil.DateOf(t.EndDate),
		UpdatedAt:  t.UpdatedAt,
		CreatedAt:  t.CreatedAt,
	}
}
