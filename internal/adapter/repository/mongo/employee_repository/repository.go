package employee_repository

import (
	"context"
	"fmt"

	"gitlab.com/zigal0/architect/pkg/logger"
	"gitlab.com/zigal0/go-reference-project/internal/business/tool/collection"
	"gitlab.com/zigal0/go-reference-project/internal/domain"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Repository struct {
	db *mongo.Collection
}

func New(db *mongo.Database) *Repository {
	return &Repository{
		db: db.Collection("employee"),
	}
}

func (repo *Repository) Trucate(ctx context.Context) error {
	_, err := repo.db.DeleteMany(ctx, bson.M{}, options.Delete())
	if err != nil {
		return fmt.Errorf(formatErrDeleteMany, err)
	}

	return nil
}

func (repo *Repository) InsertBatch(ctx context.Context, employees []domain.MongoEmployee) ([]string, error) {
	if len(employees) == 0 {
		return nil, nil
	}

	employeesDB := make([]interface{}, 0, len(employees))

	for _, employee := range employees {
		employeesDB = append(employeesDB, Employee{
			Name:      employee.Name,
			Age:       employee.Age,
			Hobbies:   employee.Hobbies,
			CreatedAt: employee.CreatedAt,
		})
	}

	res, err := repo.db.InsertMany(ctx, employeesDB, options.InsertMany())
	if err != nil {
		return nil, fmt.Errorf(formatErrInsertMany, err)
	}

	var ids []string

	for _, id := range res.InsertedIDs {
		if idBson, ok := id.(primitive.ObjectID); ok {
			ids = append(ids, idBson.Hex())
		} else {
			return nil, fmt.Errorf("get bad object id from mongo: %v", idBson)
		}
	}

	return ids, nil
}

func (repo *Repository) GetByIDs(ctx context.Context, ids []string) ([]domain.MongoEmployee, error) {
	if len(ids) == 0 {
		return nil, nil
	}

	bsonIDs := make([]primitive.ObjectID, 0, len(ids))

	for _, id := range ids {
		bsonID, err := primitive.ObjectIDFromHex(id)
		if err != nil {
			return nil, fmt.Errorf("primitive.ObjectIDFromHex: %w", err)
		}

		bsonIDs = append(bsonIDs, bsonID)
	}

	cursor, err := repo.db.Find(ctx, bson.M{"_id": bson.M{"$in": bsonIDs}}, options.Find())
	if err != nil {
		return nil, fmt.Errorf(formatErrFind, err)
	}

	defer func() {
		err := cursor.Close(ctx)
		if err != nil {
			logger.Error("failed to close: %v", err)
		}
	}()

	var employeesDB []Employee

	err = cursor.All(ctx, &employeesDB)
	if err != nil {
		return nil, fmt.Errorf("cursor.All: %w", err)
	}

	if err = cursor.Err(); err != nil {
		return nil, fmt.Errorf("cursor.Err: %w", err)
	}

	return collection.Map(employeesDB, toDomain), nil
}

func (repo *Repository) GetByID(ctx context.Context, id string) (domain.MongoEmployee, error) {
	bsonID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return domain.MongoEmployee{}, fmt.Errorf(formatErrObjectIDFromHex, err)
	}

	employeeDB := Employee{}

	filter := bson.M{"_id": bsonID}

	err = repo.db.FindOne(ctx, filter, options.FindOne()).Decode(&employeeDB)
	if err != nil {
		return domain.MongoEmployee{}, fmt.Errorf(formatErrFindOne, err)
	}

	return toDomain(employeeDB), nil
}
