package employee_repository

const (
	formatErrDeleteMany      = "db.DeleteMany: %w"
	formatErrInsertMany      = "db.InsertMany: %w"
	formatErrFind            = "db.Find: %w"
	formatErrFindOne         = "db.FindOne: %w"
	formatErrObjectIDFromHex = "ObjectIDFromHex: %w"
)
