package employee_repository

import (
	"time"

	"gitlab.com/zigal0/go-reference-project/internal/domain"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Employee struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	Name      string             `bson:"name,omitempty"`
	Age       int32              `bson:"age,omitempty"`
	Hobbies   []string           `bson:"hobbies,omitempty"`
	CreatedAt time.Time          `bson:"created_at,omitempty"`
}

func toDomain(e Employee) domain.MongoEmployee {
	return domain.MongoEmployee{
		ID:        e.ID.Hex(),
		Name:      e.Name,
		Age:       e.Age,
		Hobbies:   e.Hobbies,
		CreatedAt: e.CreatedAt,
	}
}
