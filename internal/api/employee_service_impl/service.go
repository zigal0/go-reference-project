package employee_service_impl

import (
	"context"

	gw_runtime "github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	pb "gitlab.com/zigal0/go-reference-project/internal/generated/api/employee_service"
	"google.golang.org/grpc"
)

type Service struct {
	employeeManager employeeManager

	pb.UnimplementedEmployeeServiceServer
}

func New(employeeManager employeeManager) *Service {
	return &Service{
		employeeManager: employeeManager,
	}
}

func (s *Service) RegisterGRPC(server *grpc.Server) {
	pb.RegisterEmployeeServiceServer(server, s)
}

func (s *Service) RegisterGatewayEndpoint(
	ctx context.Context,
	mux *gw_runtime.ServeMux,
	endpoint string,
	dialOptions []grpc.DialOption,
) error {
	return pb.RegisterEmployeeServiceHandlerFromEndpoint(ctx, mux, endpoint, dialOptions)
}

func (s *Service) RegisterGateway(
	ctx context.Context,
	mux *gw_runtime.ServeMux,
) error {
	return pb.RegisterEmployeeServiceHandlerServer(ctx, mux, s)
}
