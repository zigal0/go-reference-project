package employee_service_impl

import (
	"context"

	"gitlab.com/zigal0/go-reference-project/internal/domain"
)

type employeeManager interface {
	GetEmployeeWithTasks(
		ctx context.Context,
		employeeID int64,
	) (domain.EmployeeWithTasks, error)
}
