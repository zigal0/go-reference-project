package employee_service_impl

import (
	"cloud.google.com/go/civil"
	"gitlab.com/zigal0/go-reference-project/internal/domain"
	pb "gitlab.com/zigal0/go-reference-project/internal/generated/api/employee_service"
	"google.golang.org/genproto/googleapis/type/date"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func taskToPB(t domain.Task) *pb.Task {
	return &pb.Task{
		Id:         t.ID,
		EmployeeId: t.EmployeeID,
		Action:     t.Action,
		StartDate:  civilDateToPB(t.StartDate),
		EndDate:    civilDateToPB(t.EndDate),
		UpdateAt:   timestamppb.New(t.UpdatedAt),
		CreatedAt:  timestamppb.New(t.CreatedAt),
	}
}

func civilDateToPB(civilDate civil.Date) *date.Date {
	return &date.Date{
		Year:  int32(civilDate.Year),
		Month: int32(civilDate.Month),
		Day:   int32(civilDate.Day),
	}
}
