package employee_service_impl

import (
	"context"
	"fmt"

	"gitlab.com/zigal0/go-reference-project/internal/business/tool/collection"
	pb "gitlab.com/zigal0/go-reference-project/internal/generated/api/employee_service"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (s *Service) GetEmployeeWithTasksV1(
	ctx context.Context,
	req *pb.GetEmployeeWithTasksV1Request,
) (*pb.GetEmployeeWithTasksV1Response, error) {
	employeeWithTasks, err := s.employeeManager.GetEmployeeWithTasks(ctx, req.GetEmployeeId())
	if err != nil {
		return nil, fmt.Errorf("employeeManager.GetEmployeeWithTasks: %w", err)
	}

	return &pb.GetEmployeeWithTasksV1Response{
		EmployeeWithTasks: &pb.EmployeeWithTasks{
			Employee: &pb.Employee{
				Id:        employeeWithTasks.ID,
				Name:      employeeWithTasks.Name,
				Age:       employeeWithTasks.Age,
				CreatedAt: timestamppb.New(employeeWithTasks.CreatedAt),
			},
			Tasks: collection.Map(employeeWithTasks.Tasks, taskToPB),
		},
	}, nil
}
