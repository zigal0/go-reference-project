package employee_manager

import (
	"context"

	"gitlab.com/zigal0/go-reference-project/internal/domain"
)

//go:generate sh -c "rm -rf mocks && mkdir -p mocks"
//go:generate minimock -i * -o ./mocks -s "_mock.go" -g

type employeeRepo interface {
	Truncate(ctx context.Context) error
	GetByIDs(ctx context.Context, ids []int64) ([]domain.Employee, error)
	InsertBatch(ctx context.Context, employees []domain.Employee) ([]int64, error)
}

type taskRepo interface {
	Truncate(ctx context.Context) error
	GetByEmployeeID(ctx context.Context, employeeID int64) ([]domain.Task, error)
	InsertBatch(ctx context.Context, tasks []domain.Task) ([]int64, error)
	SetEmployeeID(ctx context.Context, taskID int64, employeeID int64) error
}
