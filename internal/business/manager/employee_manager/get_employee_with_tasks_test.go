package employee_manager_test

import (
	"fmt"
	"math"
	"testing"
	"time"

	"cloud.google.com/go/civil"
	"gitlab.com/zigal0/go-reference-project/internal/domain"
	"syreclabs.com/go/faker"
)

func Test_GetEmployeeWithTasks(t *testing.T) {
	t.Parallel()

	employeeID := faker.RandomInt64(1, math.MaxInt64)

	t.Run("success", func(t *testing.T) {
		t.Parallel()

		t.Run("full flow", func(t *testing.T) {
			t.Parallel()

			// arrange
			f := setUp(t)

			employee := domain.Employee{
				ID:        employeeID,
				Name:      faker.Name().FirstName(),
				Age:       int32(faker.RandomInt64(18, 150)),
				CreatedAt: time.Now(),
			}

			tasks := []domain.Task{
				{
					ID:         faker.RandomInt64(1, math.MaxInt64),
					EmployeeID: employeeID,
					Action:     faker.RandomString(10),
					StartDate:  civil.DateOf(time.Now()),
					EndDate:    civil.DateOf(time.Now()),
					UpdatedAt:  time.Now(),
					CreatedAt:  time.Now(),
				},
				{
					ID:         faker.RandomInt64(1, math.MaxInt64),
					EmployeeID: employeeID,
					Action:     faker.RandomString(10),
					StartDate:  civil.DateOf(time.Now()),
					EndDate:    civil.DateOf(time.Now()),
					UpdatedAt:  time.Now(),
					CreatedAt:  time.Now(),
				},
			}

			f.employeeRepo.GetByIDsMock.
				Expect(f.ctx, []int64{employeeID}).
				Return([]domain.Employee{employee}, nil)

			f.taskRepo.GetByEmployeeIDMock.
				Expect(f.ctx, employeeID).
				Return(tasks, nil)

			expectedEmployee := domain.EmployeeWithTasks{
				Employee: employee,
				Tasks:    tasks,
			}

			// act
			res, err := f.executor.GetEmployeeWithTasks(f.ctx, employeeID)

			// assert
			f.NoError(err)
			f.Equal(expectedEmployee, res)
		})
	})

	t.Run("fail", func(t *testing.T) {
		t.Parallel()

		t.Run("employeeRepo.GetByIDs", func(t *testing.T) {
			t.Parallel()

			// arrange
			f := setUp(t)

			f.employeeRepo.GetByIDsMock.
				Expect(f.ctx, []int64{employeeID}).
				Return(nil, errForTest)

			// act
			res, err := f.executor.GetEmployeeWithTasks(f.ctx, employeeID)

			// assert
			f.ErrorIs(err, errForTest)
			f.Empty(res)
		})

		t.Run("employee does not exist", func(t *testing.T) {
			t.Parallel()

			// arrange
			f := setUp(t)

			f.employeeRepo.GetByIDsMock.
				Expect(f.ctx, []int64{employeeID}).
				Return(nil, nil)

			expectedErr := fmt.Errorf("employee with id = '%d' does not exist", employeeID)

			// act
			res, err := f.executor.GetEmployeeWithTasks(f.ctx, employeeID)

			// assert
			f.EqualError(err, expectedErr.Error())
			f.Empty(res)
		})

		t.Run("taskRepo.GetByEmployeeID", func(t *testing.T) {
			t.Parallel()

			// arrange
			f := setUp(t)

			employee := domain.Employee{
				ID:        employeeID,
				Name:      faker.Name().FirstName(),
				Age:       int32(faker.RandomInt64(18, 150)),
				CreatedAt: time.Now(),
			}

			f.employeeRepo.GetByIDsMock.
				Expect(f.ctx, []int64{employeeID}).
				Return([]domain.Employee{employee}, nil)

			f.taskRepo.GetByEmployeeIDMock.
				Expect(f.ctx, employeeID).
				Return(nil, errForTest)

			// act
			res, err := f.executor.GetEmployeeWithTasks(f.ctx, employeeID)

			// assert
			f.ErrorIs(err, errForTest)
			f.Empty(res)
		})
	})
}
