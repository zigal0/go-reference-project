package employee_manager_test

import (
	"context"
	"errors"
	"testing"

	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
	employee_manager "gitlab.com/zigal0/go-reference-project/internal/business/manager/employee_manager"
	"gitlab.com/zigal0/go-reference-project/internal/business/manager/employee_manager/mocks"
)

var (
	errForTest = errors.New("error for test")
)

type fixture struct {
	ctx context.Context
	*assert.Assertions

	employeeRepo *mocks.EmployeeRepoMock
	taskRepo     *mocks.TaskRepoMock

	executor *employee_manager.Manager
}

func setUp(t *testing.T) (f *fixture) {
	t.Helper()

	ctrl := minimock.NewController(t)

	employeeRepo := mocks.NewEmployeeRepoMock(ctrl)
	taskRepo := mocks.NewTaskRepoMock(ctrl)

	executor := employee_manager.New(employeeRepo, taskRepo)

	return &fixture{
		ctx:        context.Background(),
		Assertions: assert.New(ctrl),

		employeeRepo: employeeRepo,
		taskRepo:     taskRepo,

		executor: executor,
	}
}
