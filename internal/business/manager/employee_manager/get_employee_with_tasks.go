package employee_manager

import (
	"context"
	"fmt"

	"gitlab.com/zigal0/architect/pkg/business_error"
	"gitlab.com/zigal0/go-reference-project/internal/domain"
)

const (
	formatErrEmployeeNotFound = "employee with id = '%d' does not exist"
)

func (m *Manager) GetEmployeeWithTasks(
	ctx context.Context,
	employeeID int64,
) (domain.EmployeeWithTasks, error) {
	employees, err := m.employeeRepo.GetByIDs(ctx, []int64{employeeID})
	if err != nil {
		return domain.EmployeeWithTasks{}, fmt.Errorf("employeeRepo.GetByIDs: %w", err)
	}

	if len(employees) == 0 {
		return domain.EmployeeWithTasks{}, business_error.New(
			fmt.Errorf(formatErrEmployeeNotFound, employeeID),
			"no employee with given id",
			business_error.NotFound,
		)
	}

	employeeWithTask := domain.EmployeeWithTasks{
		Employee: employees[0],
	}

	employeeWithTask.Tasks, err = m.taskRepo.GetByEmployeeID(ctx, employeeWithTask.ID)
	if err != nil {
		return domain.EmployeeWithTasks{}, fmt.Errorf("taskRepo.GetByEmployeeID: %w", err)
	}

	return employeeWithTask, nil
}
