package employee_manager_test

import (
	"math"
	"testing"
	"time"

	"cloud.google.com/go/civil"
	"gitlab.com/zigal0/go-reference-project/internal/domain"
	"syreclabs.com/go/faker"
)

func Test_SetEmployeesAndTasksExample(t *testing.T) {
	t.Parallel()

	var (
		employeeID1 = faker.RandomInt64(1, math.MaxInt64)
		employeeID2 = faker.RandomInt64(1, math.MaxInt64)

		taskID1 = faker.RandomInt64(1, math.MaxInt64)
		taskID2 = faker.RandomInt64(1, math.MaxInt64)
		taskID3 = faker.RandomInt64(1, math.MaxInt64)
	)

	var (
		employeesToInsert = []domain.Employee{
			{
				Name: faker.Name().FirstName(),
				Age:  int32(faker.RandomInt64(18, 150)),
			},
			{
				Name: faker.Name().FirstName(),
				Age:  int32(faker.RandomInt64(18, 150)),
			},
		}

		tasksToInsert = []domain.Task{
			{
				Action:    faker.RandomString(10),
				StartDate: civil.DateOf(time.Now()),
				EndDate:   civil.DateOf(time.Now()),
			},
			{
				Action:    faker.RandomString(10),
				StartDate: civil.DateOf(time.Now()),
				EndDate:   civil.DateOf(time.Now()),
			},
			{
				Action:    faker.RandomString(10),
				StartDate: civil.DateOf(time.Now()),
				EndDate:   civil.DateOf(time.Now()),
			},
		}
	)

	t.Run("success", func(t *testing.T) {
		t.Parallel()

		t.Run("full flow", func(t *testing.T) {
			t.Parallel()

			// arrange
			f := setUp(t)

			var ()

			f.employeeRepo.TruncateMock.
				Expect(f.ctx).
				Return(nil)

			f.taskRepo.TruncateMock.
				Expect(f.ctx).
				Return(nil)

			f.employeeRepo.InsertBatchMock.
				Expect(f.ctx, employeesToInsert).
				Return([]int64{employeeID1, employeeID2}, nil)

			f.taskRepo.InsertBatchMock.
				Expect(f.ctx, tasksToInsert).
				Return([]int64{taskID1, taskID2, taskID3}, nil)

			f.taskRepo.SetEmployeeIDMock.
				When(f.ctx, taskID1, employeeID1).
				Then(nil)

			f.taskRepo.SetEmployeeIDMock.
				When(f.ctx, taskID2, employeeID2).
				Then(nil)

			f.taskRepo.SetEmployeeIDMock.
				When(f.ctx, taskID3, employeeID1).
				Then(nil)

			// act
			err := f.executor.SetEmployeesAndTasksExample(f.ctx, employeesToInsert, tasksToInsert)

			// assert
			f.NoError(err)
		})
	})

	t.Run("fail", func(t *testing.T) {
		t.Parallel()

		t.Run("full flow", func(t *testing.T) {
			t.Parallel()

			// arrange
			f := setUp(t)

			f.employeeRepo.TruncateMock.
				Expect(f.ctx).
				Return(errForTest)

			// act
			err := f.executor.SetEmployeesAndTasksExample(f.ctx, employeesToInsert, tasksToInsert)

			// assert
			f.ErrorIs(err, errForTest)
		})
	})

	t.Run("full flow", func(t *testing.T) {
		t.Parallel()

		// arrange
		f := setUp(t)

		var ()

		f.employeeRepo.TruncateMock.
			Expect(f.ctx).
			Return(nil)

		f.taskRepo.TruncateMock.
			Expect(f.ctx).
			Return(errForTest)

		// act
		err := f.executor.SetEmployeesAndTasksExample(f.ctx, employeesToInsert, tasksToInsert)

		// assert
		f.ErrorIs(err, errForTest)
	})

	t.Run("full flow", func(t *testing.T) {
		t.Parallel()

		// arrange
		f := setUp(t)

		var ()

		f.employeeRepo.TruncateMock.
			Expect(f.ctx).
			Return(nil)

		f.taskRepo.TruncateMock.
			Expect(f.ctx).
			Return(nil)

		f.employeeRepo.InsertBatchMock.
			Expect(f.ctx, employeesToInsert).
			Return(nil, errForTest)

		// act
		err := f.executor.SetEmployeesAndTasksExample(f.ctx, employeesToInsert, tasksToInsert)

		// assert
		f.ErrorIs(err, errForTest)
	})

	t.Run("full flow", func(t *testing.T) {
		t.Parallel()

		// arrange
		f := setUp(t)

		var ()

		f.employeeRepo.TruncateMock.
			Expect(f.ctx).
			Return(nil)

		f.taskRepo.TruncateMock.
			Expect(f.ctx).
			Return(nil)

		f.employeeRepo.InsertBatchMock.
			Expect(f.ctx, employeesToInsert).
			Return([]int64{employeeID1, employeeID2}, nil)

		f.taskRepo.InsertBatchMock.
			Expect(f.ctx, tasksToInsert).
			Return(nil, errForTest)

		// act
		err := f.executor.SetEmployeesAndTasksExample(f.ctx, employeesToInsert, tasksToInsert)

		// assert
		f.ErrorIs(err, errForTest)
	})

	t.Run("full flow", func(t *testing.T) {
		t.Parallel()

		// arrange
		f := setUp(t)

		var ()

		f.employeeRepo.TruncateMock.
			Expect(f.ctx).
			Return(nil)

		f.taskRepo.TruncateMock.
			Expect(f.ctx).
			Return(nil)

		f.employeeRepo.InsertBatchMock.
			Expect(f.ctx, employeesToInsert).
			Return([]int64{employeeID1, employeeID2}, nil)

		f.taskRepo.InsertBatchMock.
			Expect(f.ctx, tasksToInsert).
			Return([]int64{taskID1, taskID2, taskID3}, nil)

		f.taskRepo.SetEmployeeIDMock.
			When(f.ctx, taskID1, employeeID1).
			Then(nil)

		f.taskRepo.SetEmployeeIDMock.
			When(f.ctx, taskID2, employeeID2).
			Then(errForTest)

		// act
		err := f.executor.SetEmployeesAndTasksExample(f.ctx, employeesToInsert, tasksToInsert)

		// assert
		f.ErrorIs(err, errForTest)
	})
}
