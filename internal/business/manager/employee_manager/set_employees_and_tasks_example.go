package employee_manager

import (
	"context"
	"fmt"

	"gitlab.com/zigal0/go-reference-project/internal/domain"
)

func (m *Manager) SetEmployeesAndTasksExample(
	ctx context.Context,
	employees []domain.Employee,
	tasks []domain.Task,
) error {
	// TODO: add work with transaction
	err := m.employeeRepo.Truncate(ctx)
	if err != nil {
		return fmt.Errorf("employeeRepo.Truncate: %w", err)
	}

	err = m.taskRepo.Truncate(ctx)
	if err != nil {
		return fmt.Errorf("taskRepo.Truncate: %w", err)
	}

	emplyeeIDs, err := m.employeeRepo.InsertBatch(ctx, employees)
	if err != nil {
		return fmt.Errorf("employeeRepo.InsertBatch: %w", err)
	}

	taskIDs, err := m.taskRepo.InsertBatch(ctx, tasks)
	if err != nil {
		return fmt.Errorf("employeeRepo.InsertBatch: %w", err)
	}

	employeeQty := len(emplyeeIDs)

	for i, taskID := range taskIDs {
		// Distribute tasks evenly between employees
		employeeID := emplyeeIDs[i%employeeQty]

		err := m.taskRepo.SetEmployeeID(ctx, taskID, employeeID)
		if err != nil {
			return fmt.Errorf("taskRepo.SetEmployeeID: %w", err)
		}
	}

	return nil
}
