package employee_manager

type Manager struct {
	employeeRepo employeeRepo
	taskRepo     taskRepo
}

func New(
	employeeRepo employeeRepo,
	taskRepo taskRepo,
) *Manager {
	return &Manager{
		employeeRepo: employeeRepo,
		taskRepo:     taskRepo,
	}
}
