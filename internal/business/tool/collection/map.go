package collection

func Map[T1 any, T2 any](input []T1, mapping func(T1) T2) []T2 {
	result := make([]T2, 0, len(input))

	for _, value := range input {
		result = append(result, mapping(value))
	}

	return result
}
