include architect.mk

#======================================#
# VARIABLES
#======================================#

## GENERAL
SERVICE_NAME = go-reference-project
MIGRATION_DIR = $(CURDIR)/migration

## BIN
GOOSE_BIN = $(LOCAL_BIN)/goose

## PG
PG_MIGRATION_DIR = $(MIGRATION_DIR)/postgresql
PG_DSN="host=127.0.0.1 port=1331 user=$(SERVICE_NAME)-user password=1234 dbname=$(SERVICE_NAME) sslmode=disable"

# MYSQL
MYSQL_MIGRATION_DIR = $(MIGRATION_DIR)/mysql
MYSQL_DSN="root:1234@tcp(localhost:1332)/$(SERVICE_NAME)?parseTime=true"

# MSSQL
MSSQL_MIGRATION_DIR = $(MIGRATION_DIR)/mssql
MSSQL_DSN="sqlserver://sa:12345Abc!@127.0.0.1:1333?database=master"

#======================================#
# INSTALLATION
#======================================#

.custom-bin-deps: export GOBIN := $(LOCAL_BIN)
.custom-bin-deps:
	$(info Installing custom bins for project...)    
	tmp=$$(mktemp -d) && cd $$tmp && go mod init temp && \
		go install github.com/pressly/goose/v3/cmd/goose@latest
		go install github.com/gojuno/minimock/v3/cmd/minimock@latest
	rm -rf $$tmp

custom-bin-deps: .custom-bin-deps ## install custom necessary bins

#======================================#
# GENERATION
#======================================#

.generate-mocks:
	$(info Generating mocks...) 
	PATH="$(LOCAL_BIN):$$PATH" go generate -x run=minimock ./...

generate-mocks: .generate-mocks ## generate mocks for tests

#======================================#
# DOCKER
#======================================#

.compose-up: 
	docker compose -p $(SERVICE_NAME) -f ./local/docker/docker-compose.yml up -d

.compose-down: 
	docker compose -p $(SERVICE_NAME) -f ./local/docker/docker-compose.yml down
 
.compose-rm:
	docker compose -p $(SERVICE_NAME) -f ./local/docker/docker-compose.yml rm -fvs  #f - force, v - any anonymous volumes, s - stop

.compose-rs:
	make compose-down
	make compose-up

compose-up: .compose-up ## start docker containers

compose-down: .compose-down ## stop docker containers

compose-rm: .compose-rm ## stop and remove docker containers

compose-rs: .compose-rs ## restart docker containers

#======================================#
# MIGRATIONS
#======================================#

## PG
.migration-pg-up: 
	$(GOOSE_BIN) -dir $(PG_MIGRATION_DIR) postgres $(PG_DSN) up

.migration-pg-down:
	$(GOOSE_BIN) -dir $(PG_MIGRATION_DIR) postgres $(PG_DSN) down

migration-pg-create: ## create migration file in pg migration folder
	$(GOOSE_BIN) -dir $(PG_MIGRATION_DIR) create $(name) sql

migration-pg-up: .migration-pg-up ## run up pg migratins

migration-pg-down: .migration-pg-down ## run down pg migratins

## MYSQL
.migration-mysql-up:
	$(GOOSE_BIN) -dir $(MYSQL_MIGRATION_DIR) mysql $(MYSQL_DSN) up

.migration-mysql-down:
	$(GOOSE_BIN) -dir $(MYSQL_MIGRATION_DIR) mysql $(MYSQL_DSN) down

migration-mysql-create: ## create migration file in mysql migration folder
	$(GOOSE_BIN) -dir $(MYSQL_MIGRATION_DIR) create $(name) sql

migration-mysql-up: .migration-mysql-up ## run up mysql migratins

migration-mysql-down: .migration-mysql-down ## run down mysql migratins

## MSSQL
.migration-mssql-up:
	$(GOOSE_BIN) -dir $(MSSQL_MIGRATION_DIR) mssql $(MSSQL_DSN) up

.migration-mssql-down:
	$(GOOSE_BIN) -dir $(MSSQL_MIGRATION_DIR) mssql $(MSSQL_DSN) down

migration-mssql-create: ## create migration file in mssql migration folder
	$(GOOSE_BIN) -dir $(MSSQL_MIGRATION_DIR) create $(name) sql

migration-mssql-up: .migration-mssql-up ## run up mssql migratins

migration-mssql-down: .migration-mssql-down ## run down mssql migratins

## General

migration-up: ## run up all migratins 
	@make .migration-pg-up 
	@make .migration-mysql-up 
	@make .migration-mssql-up

migration-down: ## run down all migratins
	@make .migration-pg-down 
	@make .migration-mysql-down 
	@make .migration-mssql-down

#======================================#
# BUILD & RUN
#======================================#

run-full: ## run service with db
	@make compose-up
	@echo "Waiting for DBs to start"
	@sleep 5
	@make migration-pg-up
	@make .run

run-db: ## run code with db examples
	go run ./cmd/db

run-grpc-server: ## run grpc-server
	go run ./cmd/grpc-server

run-grpc-client: ## run grpc-client to grpc-server
	go run ./cmd/grpc-client

run-rk-boot: ## run rk-boot example
	go run ./cmd/rk-boot

#======================================#
# .PHONY
#======================================#

.PHONY: \
	.custom-bin-deps \
    custom-bin-deps \
	.generate-mocks \
	generate-mocks \
	.compose-up \
	.compose-down \
	.compose-rm \
	.compose-rs \
	compose-up \
	compose-down \
	compose-rm \
	compose-rs \
	migration-pg-create \
	.migration-pg-up \
	migration-pg-up \
	.migration-pg-down \
	migration-pg-down \
	migration-mysql-create \
	.migration-mysql-up \
	migration-mysql-up \
	.migration-mysql-down \
	migration-mysql-down \
	migration-mssql-create \
	.migration-mssql-up \
	migration-mssql-up \
	.migration-mssql-down \
	migration-mssql-down \
	migration-up \
	migration-down \
	run-full \
	run-db \
	run-grpc-server \
	run-client \
	run-rk-boot
	