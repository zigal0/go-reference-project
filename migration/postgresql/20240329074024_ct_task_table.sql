-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS task (
    id          BIGSERIAL   PRIMARY KEY,
    employee_id BIGSERIAL,
    action      TEXT        NOT NULL,
    start_date  DATE        NOT NULL,
    end_date    DATE        NOT NULL,
    updated_at  TIMESTAMP   WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_at  TIMESTAMP   WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS task;
-- +goose StatementEnd
