-- +goose Up
-- +goose StatementBegin
IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = 'employee')
BEGIN
    CREATE TABLE employee (
        id           INT             PRIMARY KEY IDENTITY(1,1),
        name         VARCHAR(255)                                NOT NULL,
        age          INT                                         NOT NULL,
        created_at   DATETIME        DEFAULT CURRENT_TIMESTAMP   NOT NULL
    )
END
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS employee;
-- +goose StatementEnd
