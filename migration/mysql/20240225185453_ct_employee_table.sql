-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS employee (
    id          BIGINT          PRIMARY KEY     AUTO_INCREMENT,
    name        VARCHAR(255)                    NOT NULL,
    age         INT                             NOT NULL,
    created_at  TIMESTAMP       DEFAULT(NOW())  NOT NULL
);

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS employee;
-- +goose StatementEnd
