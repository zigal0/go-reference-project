package main

import (
	"context"

	"gitlab.com/zigal0/architect/pkg/closer"
	"gitlab.com/zigal0/architect/pkg/logger"
	"gitlab.com/zigal0/go-reference-project/internal/adapter/client"
	go_reference_project_client "gitlab.com/zigal0/go-reference-project/internal/adapter/client/go-reference-project"
	"go.uber.org/zap/zapcore"
)

const (
	// Сorresponds data from DB.
	employeeID = 9 // nolint: gomnd
)

func main() {
	defer closer.CloseAll()

	logger.SetLevel(zapcore.InfoLevel)

	ctx := context.Background()

	conn, err := client.ProvideGRPCConn(ctx, client.GoRegerenceProject)
	if err != nil {
		logger.Fatalf("faile to connect to go-reference-project: %v", err)
	}

	// TODO: need create global & local closer
	defer closer.Add(conn.Close)

	client := go_reference_project_client.New(conn)

	employeeWithTask, err := client.GetEmployeeWithTasks(ctx, employeeID)
	if err != nil {
		logger.Fatalf("failed to execute GetEmployeeWithTasks: %v", err)
	}

	logger.Infof("Result: %v", employeeWithTask)
}
