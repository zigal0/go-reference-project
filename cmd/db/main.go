package main

import (
	"context"

	"gitlab.com/zigal0/architect/pkg/closer"
	"gitlab.com/zigal0/architect/pkg/logger"
	"gitlab.com/zigal0/go-reference-project/config"
	"go.uber.org/zap/zapcore"
)

func main() {
	defer closer.CloseAll()

	logger.SetLevel(zapcore.InfoLevel)

	ctx := context.Background()

	err := config.InitConfig()
	if err != nil {
		logger.Fatalf("failed to init config: %v", err)
	}

	err = runMongoExample(ctx)
	if err != nil {
		logger.Fatalf("can't run runMongoExample: %v", err)
	}

	err = runPostgreSQLExample(ctx)
	if err != nil {
		logger.Fatalf("can't run runPostgreSQLExample: %v", err)
	}

	err = runMYSQLExample(ctx)
	if err != nil {
		logger.Fatalf("can't run runMYSQLExample: %v", err)
	}

	err = runMSSQLExample(ctx)
	if err != nil {
		logger.Fatalf("can't run runMSSQLExample: %v", err)
	}
}
