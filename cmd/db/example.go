// nolint: gomnd, forbidigo
package main

import (
	"context"
	"fmt"
	"time"

	_ "github.com/denisenkom/go-mssqldb"
	"github.com/go-sql-driver/mysql"
	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/jmoiron/sqlx"
	"gitlab.com/zigal0/architect/pkg/closer"
	"gitlab.com/zigal0/architect/pkg/logger"
	"gitlab.com/zigal0/go-reference-project/config"
	mongo_employee_repo "gitlab.com/zigal0/go-reference-project/internal/adapter/repository/mongo/employee_repository"
	mssql_employee_repo "gitlab.com/zigal0/go-reference-project/internal/adapter/repository/mssql/employee_repository"
	mysql_employee_repo "gitlab.com/zigal0/go-reference-project/internal/adapter/repository/mysql/employee_repository"
	postgresql_employee_repo "gitlab.com/zigal0/go-reference-project/internal/adapter/repository/postgresql/employee_repository"
	"gitlab.com/zigal0/go-reference-project/internal/domain"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func runMongoExample(ctx context.Context) error {
	logger.Info("Mongo example output:")

	mongoDSN, err := config.GetValue(config.Key_MongoDSN)
	if err != nil {
		return err
	}

	clientOptions := options.Client().ApplyURI(mongoDSN.String())

	mongoClient, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		return fmt.Errorf("mongo.Connect: %w", err)
	}

	defer func() {
		err := mongoClient.Disconnect(ctx)
		if err != nil {
			logger.Errorf("failed to close: %v", err)
		}
	}()

	err = mongoClient.Ping(ctx, readpref.Primary())
	if err != nil {
		return fmt.Errorf("mongoClient.Ping: %w", err)
	}

	mobgoDBName, err := config.GetValue(config.Key_MongoDBName)
	if err != nil {
		return err
	}

	mongoDB := mongoClient.Database(mobgoDBName.String())

	employeeRepo := mongo_employee_repo.New(mongoDB)

	err = employeeRepo.Trucate(ctx)
	if err != nil {
		return fmt.Errorf("employeeRepo.Trucate: %w", err)
	}

	employeesToInsert := []domain.MongoEmployee{
		{
			Name:      "Sergey",
			Age:       24,
			Hobbies:   []string{"skateboarding", "snowboarding"},
			CreatedAt: time.Now(),
		},
		{
			Name:      "Ilya",
			Age:       25,
			Hobbies:   []string{"skiing", "running"},
			CreatedAt: time.Now(),
		},
	}

	ids, err := employeeRepo.InsertBatch(ctx, employeesToInsert)
	if err != nil {
		return fmt.Errorf("employeeRepo.Insert: %w", err)
	}

	employees, err := employeeRepo.GetByIDs(ctx, ids)
	if err != nil {
		return fmt.Errorf("mongoEmployeeRepo.GetByIDs: %w", err)
	}

	if len(ids) != 0 {
		firstEmployee, err := employeeRepo.GetByID(ctx, ids[0])
		if err != nil {
			return fmt.Errorf("mongoEmployeeRepo.GetByID: %w", err)
		}

		logger.Infof("First employee: %v", firstEmployee)
	}

	logger.Infof("Employees from DB: %v\n", employees)

	return nil
}

func runPostgreSQLExample(ctx context.Context) error {
	logger.Info("PostgreSQL example output:")

	pgDSN, err := config.GetValue(config.Key_PGDSN)
	if err != nil {
		return err
	}

	postgresDB, err := sqlx.ConnectContext(ctx, "pgx", pgDSN.String())
	if err != nil {
		return fmt.Errorf("sqlx.Connect: %w", err)
	}

	defer closer.Add(postgresDB.Close)

	err = postgresDB.Ping()
	if err != nil {
		return fmt.Errorf("postgresDB.Ping: %w", err)
	}

	employeeRepo := postgresql_employee_repo.New(postgresDB)

	err = employeeRepo.Truncate(ctx)
	if err != nil {
		return fmt.Errorf("employeeRepo.Truncate: %w", err)
	}

	employeeToInsert := []domain.Employee{
		{
			Name: "Sergey",
			Age:  24,
		},
		{
			Name: "Ilya",
			Age:  25,
		},
	}

	ids, err := employeeRepo.InsertBatch(ctx, employeeToInsert)
	if err != nil {
		return fmt.Errorf("employeeRepo.Insert: %w", err)
	}

	employees, err := employeeRepo.GetByIDs(ctx, ids)
	if err != nil {
		return fmt.Errorf("employeeRepo.GetByIDs: %w", err)
	}

	logger.Infof("Employees from DB: %v\n", employees)

	return nil
}

func runMYSQLExample(ctx context.Context) error {
	logger.Info("MYSQL example output:")

	cfg := mysql.NewConfig()

	user, err := config.GetValue(config.Key_MYSQLUser)
	if err != nil {
		return err
	}

	password, err := config.GetValue(config.Key_MYSQLPassword)
	if err != nil {
		return err
	}

	dbName, err := config.GetValue(config.Key_MYSQLDBName)
	if err != nil {
		return err
	}

	address, err := config.GetValue(config.Key_MYSQLAddress)
	if err != nil {
		return err
	}

	cfg.User = user.String()
	cfg.Passwd = password.String()
	cfg.DBName = dbName.String()
	cfg.Addr = address.String()
	cfg.Net = "tcp"
	cfg.ParseTime = true

	mysqlDB, err := sqlx.ConnectContext(ctx, "mysql", cfg.FormatDSN())
	if err != nil {
		return fmt.Errorf("sqlx.Connect: %w", err)
	}

	defer closer.Add(mysqlDB.Close)

	pingErr := mysqlDB.Ping()
	if pingErr != nil {
		return fmt.Errorf("mysqlDB.Ping: %w", err)
	}

	employeeRepo := mysql_employee_repo.New(mysqlDB)

	err = employeeRepo.Truncate(ctx)
	if err != nil {
		return fmt.Errorf("employeeRepo.Truncate: %w", err)
	}

	employeeToInsert := []domain.Employee{
		{
			Name: "Sergey",
			Age:  24,
		},
		{
			Name: "Ilya",
			Age:  25,
		},
	}

	ids, err := employeeRepo.InsertBatch(ctx, employeeToInsert)
	if err != nil {
		return fmt.Errorf("employeeRepo.Insert: %w", err)
	}

	employees, err := employeeRepo.GetByIDs(ctx, ids)
	if err != nil {
		return fmt.Errorf("employeeRepo.GetByIDs: %w", err)
	}

	logger.Infof("Employees from DB: %v\n", employees)

	return nil
}

func runMSSQLExample(ctx context.Context) error {
	logger.Info("MSSQL example output:")

	dsn, err := config.GetValue(config.Key_MSSQLDSN)
	if err != nil {
		return err
	}

	mssqlDB, err := sqlx.ConnectContext(ctx, "sqlserver", dsn.String())
	if err != nil {
		return fmt.Errorf("sqlx.Connect: %w", err)
	}

	defer closer.Add(mssqlDB.Close)

	pingErr := mssqlDB.Ping()
	if pingErr != nil {
		return fmt.Errorf("mssqlDB.Ping: %w", err)
	}

	employeeRepo := mssql_employee_repo.New(mssqlDB)

	err = employeeRepo.Truncate(ctx)
	if err != nil {
		return fmt.Errorf("employeeRepo.Truncate: %w", err)
	}

	employeeToInsert := []domain.Employee{
		{
			Name: "Sergey",
			Age:  24,
		},
		{
			Name: "Ilya",
			Age:  25,
		},
	}

	ids, err := employeeRepo.InsertBatch(ctx, employeeToInsert)
	if err != nil {
		return fmt.Errorf("employeeRepo.Insert: %w", err)
	}

	employees, err := employeeRepo.GetByIDs(ctx, ids)
	if err != nil {
		return fmt.Errorf("employeeRepo.GetByIDs: %w", err)
	}

	logger.Infof("Employees from DB: %v\n", employees)

	return nil
}
