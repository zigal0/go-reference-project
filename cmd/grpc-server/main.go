package main

import (
	"context"
	"net"

	"cloud.google.com/go/civil"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/validator"
	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/jmoiron/sqlx"
	"gitlab.com/zigal0/architect/pkg/closer"
	"gitlab.com/zigal0/architect/pkg/logger"
	"gitlab.com/zigal0/go-reference-project/config"
	employee_repo "gitlab.com/zigal0/go-reference-project/internal/adapter/repository/postgresql/employee_repository"
	task_repo "gitlab.com/zigal0/go-reference-project/internal/adapter/repository/postgresql/task_repository"
	"gitlab.com/zigal0/go-reference-project/internal/api/employee_service_impl"
	employee_manager "gitlab.com/zigal0/go-reference-project/internal/business/manager/employee_manager"
	"gitlab.com/zigal0/go-reference-project/internal/domain"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc"
)

// nolint: gomnd
func main() {
	defer closer.CloseAll()

	logger.SetLevel(zapcore.InfoLevel)

	ctx := context.Background()

	err := config.InitConfig()
	if err != nil {
		logger.Fatalf("failed to init config: %v", err)
	}

	pgDSN, err := config.GetValue(config.Key_PGDSN)
	if err != nil {
		logger.Fatal(err)
	}

	postgresDB, err := sqlx.ConnectContext(ctx, "pgx", pgDSN.String())
	if err != nil {
		logger.Fatalf("sqlx.Connect: %v", err)
	}

	defer closer.Add(postgresDB.Close)

	err = postgresDB.Ping()
	if err != nil {
		logger.Fatalf("postgresDB.Ping: %v", err)
	}

	employeeRepo := employee_repo.New(postgresDB)
	taskRepo := task_repo.New(postgresDB)

	employeeMng := employee_manager.New(employeeRepo, taskRepo)

	employeesToInsert := []domain.Employee{
		{
			Name: "Sergey",
			Age:  24,
		},
		{
			Name: "Ilya",
			Age:  25,
		},
	}

	tasksToInsert := []domain.Task{
		{
			Action:    "wash the floor",
			StartDate: civil.Date{Year: 2024, Month: 3, Day: 10},
			EndDate:   civil.Date{Year: 2024, Month: 3, Day: 11},
		},
		{
			Action:    "throw out the trash",
			StartDate: civil.Date{Year: 2024, Month: 3, Day: 10},
			EndDate:   civil.Date{Year: 2024, Month: 3, Day: 12},
		},
		{
			Action:    "sweep the floor",
			StartDate: civil.Date{Year: 2024, Month: 3, Day: 10},
			EndDate:   civil.Date{Year: 2024, Month: 3, Day: 13},
		},
	}

	err = employeeMng.SetEmployeesAndTasksExample(ctx, employeesToInsert, tasksToInsert)
	if err != nil {
		logger.Fatalf("failed to set employees and tasks for example: %v", err)
	}

	employeeSvc := employee_service_impl.New(employeeMng)

	grpcServer := grpc.NewServer(
		grpc.ChainUnaryInterceptor(
			grpc_validator.UnaryServerInterceptor(),
			grpc_recovery.UnaryServerInterceptor(),
		),
	)

	employeeSvc.RegisterGRPC(grpcServer)

	listener, err := net.Listen("tcp", "localhost:7000")
	if err != nil {
		logger.Fatalf("failed to listen: %v", err)
	}

	logger.Infof("server listening at %v", listener.Addr())

	if err := grpcServer.Serve(listener); err != nil {
		logger.Fatalf("failed to serve: %v", err)
	}
}
