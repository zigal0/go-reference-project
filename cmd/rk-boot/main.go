package main

import (
	"context"

	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/jmoiron/sqlx"
	rkboot "github.com/rookie-ninja/rk-boot"
	rkgrpc "github.com/rookie-ninja/rk-grpc/boot"
	"gitlab.com/zigal0/architect/pkg/closer"
	"gitlab.com/zigal0/architect/pkg/logger"
	"gitlab.com/zigal0/go-reference-project/config"
	employee_repo "gitlab.com/zigal0/go-reference-project/internal/adapter/repository/postgresql/employee_repository"
	task_repo "gitlab.com/zigal0/go-reference-project/internal/adapter/repository/postgresql/task_repository"
	"gitlab.com/zigal0/go-reference-project/internal/api/employee_service_impl"
	employee_manager "gitlab.com/zigal0/go-reference-project/internal/business/manager/employee_manager"
	"go.uber.org/zap/zapcore"
)

func main() {
	defer closer.CloseAll()

	logger.SetLevel(zapcore.InfoLevel)

	ctx := context.Background()

	err := config.InitConfig()
	if err != nil {
		logger.Fatalf("failed to init config: %v", err)
	}

	pgDSN, err := config.GetValue(config.Key_PGDSN)
	if err != nil {
		logger.Fatal(err)
	}

	postgresDB, err := sqlx.ConnectContext(ctx, "pgx", pgDSN.String())
	if err != nil {
		logger.Fatalf("sqlx.Connect: %v", err)
	}

	defer closer.Add(postgresDB.Close)

	err = postgresDB.Ping()
	if err != nil {
		logger.Fatalf("postgresDB.Ping: %v", err)
	}

	employeeRepo := employee_repo.New(postgresDB)
	taskRepo := task_repo.New(postgresDB)

	employeeMng := employee_manager.New(employeeRepo, taskRepo)

	employeeSvc := employee_service_impl.New(employeeMng)

	boot := rkboot.NewBoot()

	boot.GetAppInfoEntry()

	grpcEntry, ok := boot.GetEntry("employee_service").(*rkgrpc.GrpcEntry)
	if !ok {
		logger.Fatalf("failed to convert grpcEntry: %v", err)
	}

	grpcEntry.AddRegFuncGrpc(employeeSvc.RegisterGRPC)
	grpcEntry.AddRegFuncGw(employeeSvc.RegisterGatewayEndpoint)

	boot.Bootstrap(ctx)

	boot.WaitForShutdownSig(ctx)
}
